import {
  Component,
  AfterViewInit
} from '@angular/core';
import * as L from 'leaflet'
import { RegionGEOJSON } from '../../assets/GeoJson/region';
import { DepartementGEOJSON } from '../../assets/GeoJson/departement';
import { CommuneGEOJSON } from '../../assets/GeoJson/commune';
import 'leaflet-easyprint'
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  map: any;

  // retrieve from https://gist.github.com/ThomasG77/61fa02b35abf4b971390
  smallIcon = new L.Icon({
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon.png',
    iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon-2x.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    shadowSize: [41, 41]
  });

  constructor() {}

  ngAfterViewInit(): void {
    this.createMap();
  }
  
  

  createMap() {
    const parcThabor = {
      lat: 47.316305347055184,
      lng: 5.094099328428697
    };

    const zoomLevel = 15;

    this.map = L.map('map', {
      center: [parcThabor.lat, parcThabor.lng],
      zoom: zoomLevel,
      minZoom: 3
    });

    const osmMap = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '© OpenStreetMap contributors',
      maxZoom: 19,
    });


    var PlanIGNV2 = L.tileLayer(
      "https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?" +
      "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0" +
      "&STYLE=normal" +
      "&TILEMATRIXSET=PM" +
      "&FORMAT=image/png"+
      "&LAYER=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2"+
      "&TILEMATRIX={z}" +
      "&TILEROW={y}" +
      "&TILECOL={x}",
      {
          minZoom : 0,
          maxZoom : 18,
                  attribution : "IGN-F/Geoportail",
          tileSize : 256 // les tuiles du Géooportail font 256x256px
      }
  );

    var GeoportailOrtho = L.tileLayer('https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}', {
      attribution: '<a target="_blank" href="https://www.geoportail.gouv.fr/">Geoportail France</a>',
      bounds: [
        [-75, -180],
        [81, 180]
      ],
      minZoom: 2,
      maxZoom: 19,
    });
    // var NasaNightView = L.tileLayer('https://map1.vis.earthdata.nasa.gov/wmts-webmerc/VIIRS_CityLights_2012/default//GoogleMapsCompatible_Level{maxZoom}/{z}/{y}/{x}.jpg', {
    //   attribution: 'Imagery provided by services from the Global Imagery Browse Services (GIBS), operated by the NASA/GSFC/Earth Science Data and Information System (<a href="https://earthdata.nasa.gov">ESDIS</a>) with funding provided by NASA/HQ.',
    //   bounds: [
    //     [-85.0511287776, -179.999999975],
    //     [85.0511287776, 179.999999975]
    //   ],
    //   minZoom: 1,
    //   maxZoom: 8,
    // });
    var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
      maxZoom: 17,
      attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
    });

    var myStyle = {
      "color": "#ff7800",
      "weight": 5,
      "opacity": 0.65
  };
  
    var Region = L.geoJSON(JSON.parse(RegionGEOJSON), {style: {"color": "#ff7800","weight": 5,"opacity": 0.65},
      onEachFeature: function (feature, featureLayer) {
        featureLayer.bindPopup(feature.properties.nom);
      }
    })

    var Departement = L.geoJSON(JSON.parse(DepartementGEOJSON), {style: {"color": "#ffff00","weight": 5,"opacity": 0.65},
      onEachFeature: function (feature, featureLayer) {
        featureLayer.bindPopup(feature.properties.nom);
      }
    })
    var Commune = L.geoJSON(JSON.parse(CommuneGEOJSON), {style: {"color": "#df56ff","weight": 5,"opacity": 0.65},
      onEachFeature: function (feature, featureLayer) {
        featureLayer.bindPopup(feature.properties.nom);
      }
    })

    var Topography= L.tileLayer.wms('http://geoservices.brgm.fr/WMS-C/?', {
        layers: 'GEOLOGIE'
    });
    // var Places= L.tileLayer.wms('http://ows.mundialis.de/services/service?', {
		// 	layers: 'TOPO-WMS,OSM-Overlay-WMS'
		// });


    var ALEARG_REALISE= L.tileLayer.wms('http://georisques.gouv.fr/services?', {
      layers: 'ALEARG_REALISE',
      minZoom: 0,
      maxZoom: 8,
      transparent: true,
      format: 'image/png'   
  });

 var CAVITE_LOCALISEE= L.tileLayer.wms('http://georisques.gouv.fr/services?', {
      layers: 'CAVITE_LOCALISEE',
      minZoom: 0,
      maxZoom: 8,
      transparent: true,
      format: 'image/png'   
  });


    var baseMaps = {
      'osmMap': osmMap,
      'PlanIGNV2':PlanIGNV2,
      'GeoportailOrtho': GeoportailOrtho,
      // 'NasaNightView': NasaNightView,
      'OpenTopoMap': OpenTopoMap,
      'Topography': Topography,
      // 'Place': Places,
    }
    var overlayMaps = {
      'ALEARG_REALISE':ALEARG_REALISE,
      'CAVITE_LOCALISEE':CAVITE_LOCALISEE,
      'Region': Region,
      'Departement': Departement,
      'Commune': Commune
    }

      // var printer = L.easyPrint({
      //     tileLayer: baseMaps,
      //     sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      //     filename: 'myMap',
      //     exportOnly: true,
      //     hideControlContainer: true
      // }).addTo(this.map);
      // printer.printMap('CurrentSize', 'MyManualPrint')


    var geotecMarker = L.marker([47.316583080516104, 5.094190365622627], {
      title: 'GEOTEC',
      icon: this.smallIcon
    }).addTo(this.map);
    geotecMarker.bindPopup("<div style='text-align:center; margin-top:0px; margin-bottom:0px;'><p style='color:#e87511'><b>Geotec</b></p><p style=' margin-top:0px; margin-bottom:0px;'>2 rue champeau</p><p>47.316583, 5.094190</p><div><p><a href='https://geotec.fr/'>Site Géotec</a></p></div>");
    geotecMarker.bindTooltip("Agence geotec");
    PlanIGNV2.addTo(this.map)
    L.control.scale().addTo(this.map);
    L.control.layers(baseMaps, overlayMaps).addTo(this.map);

  }

}